extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var thrust = 0
export var maxThrust = 2
var anchor

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	anchor = get_global_transform().origin - get_parent().get_global_transform().origin;
	get_parent().apply_impulse(anchor, get_parent().transform.basis.y.normalized() * thrust * delta);
	
