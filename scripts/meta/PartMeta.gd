extends Spatial

export(String) var partName
export(String, MULTILINE) var partDescription
export(String) var partManufacturer
export(NodePath) onready var anchors;
export(bool) var radial

var path = []
export var poffest = Vector3.ZERO
