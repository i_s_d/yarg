extends Spatial


export var speed = 0.1

func _input(event):
	if event is InputEventMouseButton:
		if event.pressed and event.button_index == BUTTON_WHEEL_UP:
			translate(Vector3(0, speed, 0))
		elif event.pressed and event.button_index == BUTTON_WHEEL_DOWN:
			translate(Vector3(0, -speed, 0))

# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
