extends Spatial

# Currently selected part
export(PackedScene) var selectedPart;

# The camera to perform raycasts
onready var camera = $"Camera mover/Camera"

# Radial attachment ray length
const ray_length = 50
# Distance to place not snapped part
const part_from_camera = 20
# Anchor attachment ray
const anchor_ray_length = 20

# Current part anchors
var partAnchors

# Loaded ship data
var assembly = {
	'name': 'fuel',
	'offset': Vector3(0, 0, 0),
	'attachments': [
		{ 'name': 'fuel', 'attachments': [], 'offset': Vector3(0, 3, 0) }
	]
}

# Attached anchors
onready var attached = []

# Currently placed part
var newPart: RigidBody = null

# Load ship from data
func loadShip(part=assembly, parent=self, path=[]):
	var asset = (load('res://parts/' + part.name + '.tscn') as PackedScene).instance() as Spatial
	parent.add_child(asset)
	asset.transform.origin = part.offset
	attached.append_array(getAnchors(asset))
	asset.path = path
	asset.mode = RigidBody.MODE_KINEMATIC
	for i in range(len(part.attachments)):
		loadShip(part.attachments[i], asset, path + [i])
	

# Get anchors of new part
func getAnchors(part):
	return part.get_node(part.anchors).get_children()

# Cast from camera to attach radial
func cast(event):
	var from = camera.project_ray_origin(event.position)
	var to = from + camera.project_ray_normal(event.position) * ray_length
	var space_state = get_world().direct_space_state
	return space_state.intersect_ray(from, to, [newPart], 1)

# Cast back to define how far we
# should offset part when attaching it radially
func castback(pastcast, current):
	var dest = current
	var from = dest - pastcast.normal * ray_length
	var to = dest + pastcast.normal * ray_length
	var space_state = get_world().direct_space_state
	return space_state.intersect_ray(from, to, [], 4)

# Get curs position in 3D world
func getCurs3D(event):
	var from = camera.project_ray_origin(event.position)
	return from + camera.project_ray_normal(event.position) * part_from_camera

# Get collided part
func getOwner(cast):
	var owner = (cast.collider as Spatial).shape_find_owner(cast.shape)
	return (cast.collider as Spatial).shape_owner_get_owner(owner) as Spatial

func changePart(changeTo: PackedScene):
	selectedPart = changeTo
	# Remove old part
	if newPart:
		newPart.queue_free()
	# Create new instance
	newPart = selectedPart.instance()
	add_child(newPart)
	partAnchors = getAnchors(newPart)
	changePartLayer(true)
	newPart.contact_monitor = true
	newPart.contacts_reported = 1
	newPart.mode = RigidBody.MODE_KINEMATIC

func changePartLayer(on):
	(newPart as RigidBody).collision_layer = (4 if on else 1)
	(newPart as RigidBody).collision_mask = (5 if on else 1)
	var anchors = newPart.get_node(newPart.anchors) as Area
	anchors.collision_layer = (4 if on else 2)
	anchors.collision_mask = (5 if on else 2)

func getBasePart(obj: Spatial) -> Spatial:
	var res = obj
	while not res is RigidBody and res != null:
		res = res.get_parent()
	return res

func attachPart(to: Spatial):
	var attachments = assembly.attachments
	for i in to.path:
		attachments = attachments[i].attachments
	var resource = ((selectedPart as PackedScene).get_path() as String).trim_suffix('.tscn').split('/')[-1]
	to.add_child(newPart)
	attachments.append({
		'name': (selectedPart as PackedScene).resource_name,
		'offset': newPart.transform.origin,
		'attachments': []
	})
	changePartLayer(false)
	attached += [] + partAnchors
	partAnchors = []
	newPart = null
	print('Attach to ', to.path, ' "', resource, '"')

func placePart(event, attach=false):
	if newPart == null:
		return null
	var result = null
	if newPart.radial:
		result = cast(event)
		if result:
			# Radially attach
			var res = castback(result, newPart.global_transform.origin)
			var shift = Vector3.ZERO
			if res:
				shift = newPart.global_transform.origin - res.position
			newPart.global_transform.origin = result.position + shift
			var norm = (result.normal as Vector3)
			norm.y = 0
			norm = norm.normalized()
			if norm.length():
				newPart.look_at(newPart.global_transform.origin - norm, newPart.global_transform.basis.y.normalized());
			#newPart.look_at(newPart.global_transform.origin + Vector3.LEFT,  newPart.global_transform.basis.y.normalized())
			#var res = castback(event, result)
			
			#newPart.force_update_transform()
			#var collisions = (newPart as RigidBody).get_colliding_bodies()
			#if collisions:
			#	print('collide ', collisions)
			if attach:
				#print(shift)
				attachPart(getBasePart(getOwner(result)))
				return
	if !result:
		# Put somethere in 3D world
		var pos = getCurs3D(event)
		newPart.transform.origin = pos
		newPart.transform.basis = Basis(Vector3.UP)
		newPart.force_update_transform()
		for i in partAnchors:
			# Try to anchor
			var anchorPos = i.global_transform.origin
			var spos = camera.unproject_position(anchorPos)
			var start = camera.project_ray_origin(spos)
			var end = anchorPos + (anchorPos - start).normalized() * anchor_ray_length
			
			var space_state = get_world().direct_space_state
			var intersect = space_state.intersect_ray(start, end, [newPart], 2, false, true)
			if intersect:
				# Anchored
				var owner = getOwner(intersect)
				var offset = newPart.global_transform.origin - anchorPos
				newPart.transform.origin = owner.global_transform.origin + offset
				newPart.force_update_transform()
				if attach:
					attachPart(getBasePart(owner))
					return

func _ready():
	loadShip()

var evt = null
var place = false

var count = 1

func _physics_process(delta: float) -> void:
	if not evt:
		return
	placePart(evt, place)

func _input(event):
	if event is InputEventMouseMotion:
		place = false
		evt = event
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed == true:
		place = true
		evt = event
