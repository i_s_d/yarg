extends Button

signal partButtonHovered

export(PackedScene) var part

onready var vab : Spatial = get_tree().get_current_scene()

func _ready():
	text = part.instance().partName

func _process(delta: float) -> void:
	if is_hovered():
		emit_signal("partButtonHovered", part)

func _pressed():
	vab.changePart(part)
