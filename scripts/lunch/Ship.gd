extends RigidBody


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var thrust = 0;

var bar : ProgressBar;

export var speed = 0.1;

export var inputs = [];
export var outputs = {
	'throttle': []
}

# Called when the node enters the scene tree for the first time.
func _ready():
	bar = get_parent().find_node('ThrottleBar')


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func triggerEvent(name : str, val = null):
	for i in outputs[name]:
		i(val)

func setThrust(nthrust : float):
	thrust = nthrust
	if thrust > 1: thrust = 1
	if thrust < 0: thrust = 0
	bar.value = thrust * 100
	$Engine.thrust = thrust


func _input(event):
	if event is InputEventKey:
		if event.pressed and event.scancode == KEY_SHIFT:
			setThrust(thrust + speed)
		elif event.pressed and event.scancode == KEY_CONTROL:
			setThrust(thrust - speed)
		elif event.pressed and event.scancode == KEY_Z:
			setThrust(1)
		elif event.pressed and event.scancode == KEY_X:
			setThrust(0)
